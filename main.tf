provider "aws"{
    region = "us-east-1"
}

variable vpc_cidr_block{}
variable subnet_cidr_block{}
variable available_zone{}
variable env_prefix{}
variable myip{}
variable instance_type{}
variable ra_master_pub_key_location{}

resource "aws_vpc" "ra-vpc"{
    cidr_block = var.vpc_cidr_block
    tags = {
        Name: "${var.env_prefix}-vpc"
    }
}

resource "aws_subnet" "ra-subnet-1"{
    vpc_id = aws_vpc.ra-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.available_zone
    tags = {
        Name: "${var.env_prefix}-subnet-1"
    }
}

resource "aws_internet_gateway" "ra-igw"{
    vpc_id = aws_vpc.ra-vpc.id
    tags = {
        Name: "${var.env_prefix}-ra-igw"
    }
}

resource "aws_default_route_table" "main-ra-rtb"{
    default_route_table_id = aws_vpc.ra-vpc.default_route_table_id
    
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.ra-igw.id
    }

    tags = {
        Name: "${var.env_prefix}-ra-main-rtb"
    }
}

resource "aws_default_security_group" "default-sg"{
    vpc_id = aws_vpc.ra-vpc.id

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.myip]
    }

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = []
    }

    tags = {
        Name: "${var.env_prefix}-ra-sg"
    }
}

data "aws_ami" "latest-amazon-linux-image"{
    most_recent = true
    owners = ["amazon"]
    
    filter {
        name = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]
    }

    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }
}

output "o_aws_ami_id" {
  value = data.aws_ami.latest-amazon-linux-image.id
}

output "o_ec2_public_ip" {
  value = aws_instance.ra-server.public_ip
}

resource "aws_instance" "ra-server"{
    ami = data.aws_ami.latest-amazon-linux-image.id
    instance_type = var.instance_type

    subnet_id = aws_subnet.ra-subnet-1.id
    vpc_security_group_ids = [aws_default_security_group.default-sg.id]
    availability_zone = var.available_zone

    associate_public_ip_address = true
    key_name = aws_key_pair.ssh-keypair.key_name

    user_data = file("entry-point-script.sh")

    tags = {
        Name: "${var.env_prefix}-ra-server"
    }
}

resource "aws_key_pair" "ssh-keypair"{
    key_name = "ra-master-key"
    public_key = file(var.ra_master_pub_key_location)
}